package it.unibo.trashware.persistence.model.people;

import it.unibo.trashware.persistence.repository.metamapping.annotations.EntityInterface;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceMethodToSchemaField;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceToSchemaEntity;

/**
 * This interface describes the category assignable to a {@link TrashwareWorker}.
 * 
 * @author Manuel Bonarrigo
 *
 */
@EntityInterface
@InterfaceToSchemaEntity(schemaEntity = "WorkerCategories")
public interface TrashwareWorkerCategory extends Comparable<TrashwareWorkerCategory> {

    /**
     * Retrieve the name of this category, an information expressing what kind of job the worker does.
     * 
     * @return a {@link String} with the name of the category.
     */
    @InterfaceMethodToSchemaField(returnType = String.class, schemaField = "Name")
    String getName();

}
