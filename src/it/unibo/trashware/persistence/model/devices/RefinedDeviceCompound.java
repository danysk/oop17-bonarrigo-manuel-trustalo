package it.unibo.trashware.persistence.model.devices;

import it.unibo.trashware.persistence.repository.metamapping.annotations.Carrier;
import it.unibo.trashware.persistence.repository.metamapping.annotations.EntityInterface;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceMethodToSchemaField;
import it.unibo.trashware.persistence.repository.metamapping.annotations.InterfaceToSchemaEntity;

/**
 * Express the composition of two {@link RefinedDevice}.
 * 
 * @author Manuel Bonarrigo
 *
 */
@EntityInterface
@Carrier
@InterfaceToSchemaEntity(schemaEntity = "DevicesWithIDComponentOfDeviceWithID")
public interface RefinedDeviceCompound {

    /**
     * Retrieve the {@link RefinedDevice} which is supposed to be the main component
     * of the compound.
     * 
     * @return a {@link RefinedDevice} ought to be the main component.
     */
    @InterfaceMethodToSchemaField(returnType = RefinedDevice.class, schemaField = "Compound")
    RefinedDevice getCompound();

    /**
     * Retrieve the {@link RefinedDevice} which is supposed to be the minor component
     * of the compound.
     * 
     * @return a {@link RefinedDevice} ought to be the minor component.
     */
    @InterfaceMethodToSchemaField(returnType = RefinedDevice.class, schemaField = "Component")
    RefinedDevice getComponent();

}
