package it.unibo.trashware.persistence.repository.crud.entities.devices.taxonomy;

import java.util.Set;

import it.unibo.trashware.persistence.model.otherdevices.FrequencyUnit;
import it.unibo.trashware.persistence.repository.exception.BoundedReferenceException;
import it.unibo.trashware.persistence.repository.exception.DuplicateKeyValueException;
import it.unibo.trashware.persistence.repository.exception.NonExistentReferenceException;
import it.unibo.trashware.persistence.repository.query.criteria.QueryObject;

/**
 * The interface modeling the four CRUD operations for the domain entity of
 * {@link FrequencyUnit}.
 * 
 * @author Manuel Bonarrigo
 */
public interface PersistentFrequencyUnit {
    /**
     * The CRUD operation of proposing a new {@link FrequencyUnit} to be created.
     * 
     * @param unit
     *            the FrequencyUnit to be created.
     * @throws DuplicateKeyValueException
     *             if the FrequencyUnit to be created is already present, and the
     *             persistence storage does not tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void createEntry(FrequencyUnit unit) throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of requesting a {@link Set} of {@link FrequencyUnit}
     * filtered by the conditions of the {@link QueryObject}.
     * 
     * @param filter
     *            the filter that will determine which results are going to be
     *            fetched
     * @return A Set containing all the FrequencyUnit objects matched against the
     *         filter
     */
    Set<FrequencyUnit> readFrequencyUnits(QueryObject filter);

    /**
     * The CRUD operation of proposing a {@link FrequencyUnit} to be updated with
     * the value of a new one.
     * 
     * @param oldUnit
     *            the FrequencyUnit actually stored.
     * @param newUnit
     *            the FrequencyUnit with the informations to be fetched for update.
     * @throws DuplicateKeyValueException
     *             if the FrequencyUnit update transforms the entity in an already
     *             present one, and the persistence storage does not tolerate value
     *             repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * 
     */
    void updateEntry(FrequencyUnit oldUnit, FrequencyUnit newUnit) throws NonExistentReferenceException, 
            DuplicateKeyValueException;

    /**
     * The CRUD operation of proposing a {@link FrequencyUnit} for deletion.
     * 
     * @param unit
     *            the FrequencyUnit to be deleted from the persistent data.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * @throws BoundedReferenceException
     *             if exists any entity actively referencing this one, making thus
     *             this one impossible to delete.
     */
    void deleteEntry(FrequencyUnit unit) throws NonExistentReferenceException, BoundedReferenceException;

}
