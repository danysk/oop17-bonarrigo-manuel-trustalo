package it.unibo.trashware.persistence.repository.crud.entities.people;

import java.util.Set;

import it.unibo.trashware.persistence.model.people.PersonCategory;
import it.unibo.trashware.persistence.repository.exception.BoundedReferenceException;
import it.unibo.trashware.persistence.repository.exception.DuplicateKeyValueException;
import it.unibo.trashware.persistence.repository.exception.NonExistentReferenceException;
import it.unibo.trashware.persistence.repository.query.criteria.QueryObject;

/**
 * The interface modeling the four CRUD operations for the domain entity of
 * {@link PersonCategory}.
 * 
 * @author Manuel Bonarrigo
 */
public interface PersistentJuridicalPersonCategory {

    /**
     * The CRUD operation of proposing a new {@link PersonCategory} to be created.
     * 
     * @param category
     *            the JuridicalPersonCategory to be created
     * @throws DuplicateKeyValueException
     *             if the TrashwareWorker to be created is already present, and the
     *             persistence storage does not tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     */
    void createEntry(PersonCategory category) throws NonExistentReferenceException, DuplicateKeyValueException;

    /**
     * The CRUD operation of requesting a {@link Set} of {@link PersonCategory}
     * filtered by the conditions of the {@link QueryObject}.
     * 
     * @param filter
     *            the filter that will determine which results are going to be
     *            fetched
     * @return A Set containing all the JuridicalPersonCategory objects matched
     *         against the filter
     */
    Set<PersonCategory> readJuridicalPersonCategories(QueryObject filter);

    /**
     * The CRUD operation of proposing a {@link PersonCategory} to be updated with
     * the value of a new one.
     * 
     * @param oldCategory
     *            the JuridicalPersonCategory actually stored.
     * @param newCategory
     *            the JuridicalPersonCategory with the informations to be fetched
     *            for update.
     * @throws DuplicateKeyValueException
     *             if the JuridicalPersonCategory update transforms the entity in an
     *             already present one, and the persistence storage does not
     *             tolerate value repetition.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     */
    void updateEntry(PersonCategory oldCategory, PersonCategory newCategory) throws NonExistentReferenceException, 
            DuplicateKeyValueException;

    /**
     * The CRUD operation of proposing a {@link PersonCategory} for deletion.
     * 
     * @param category
     *            the JuridicalPersonCategory to be deleted from the persistent
     *            data.
     * @throws NonExistentReferenceException
     *             if any referenced value is absent or malformed, and the
     *             persistence storage was not able to precisely locate it.
     * @throws BoundedReferenceException
     *             if exists any entity actively referencing this one, making thus
     *             this one impossible to delete.
     */
    void deleteEntry(PersonCategory category) throws NonExistentReferenceException, BoundedReferenceException;
}
