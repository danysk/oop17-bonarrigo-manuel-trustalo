package it.unibo.trashware.persistence.repository.crud.domain;

import it.unibo.trashware.persistence.repository.crud.RequestDispatcher;

/**
 * Marker interface which joins the {@link RequestDispatcher} behaviour over the
 * {@link CrudPeople} one.
 * <p>
 * Makes the implementor able to (and constrained of) correlate the ability of
 * being entrusted with the dispatching of requests while also actually pursuing
 * the CRUD operations over whatsoever persistence technology he is able to
 * operate onto.
 * <p>
 * Implementors needs to resolve any discrepancy between the semantically
 * related interfaces methods.
 *
 * @author Manuel Bonarrigo
 */
public interface PeopleDomain extends RequestDispatcher, CrudPeople {

}
